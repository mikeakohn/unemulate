/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#include <stdint.h>

struct Table6502
{
  const char *name;
  uint8_t len;
  uint8_t branch;
};

extern struct Table6502 table_6502[];

