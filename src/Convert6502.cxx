/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "Convert6502.h"
#include "Generate68000.h"
#include "Table6502.h"

Convert6502::Convert6502() :
  bank(0)
{
  generate = new Generate68000();
}

Convert6502::~Convert6502()
{
  delete generate;
}

int Convert6502::process(uint8_t *code)
{
  char filename[128];
  int rst_vector = code[16384-4] | (code[16384-3] << 8);
  int brk_vector = code[16384-2] | (code[16384-1] << 8);
  int start = -1;
  int n;

  printf("Reset Vector: 0x%04x (%d)\n", rst_vector, rst_vector);
  printf("Break Vector: 0x%04x (%d)\n", brk_vector, brk_vector);

  memset(has_code, 0, sizeof(has_code));
  instruction_count = 0;
  code_length = 0;

  find_blocks(code, rst_vector);
  find_blocks(code, brk_vector);

  printf("instruction_count=%d\n", instruction_count);
  printf("code_length=%d\n", code_length);

  sprintf(filename, "bank_%d.asm", bank);
  FILE *fp = fopen(filename, "wb");

  if (fp == NULL)
  {
    printf("Can't open file %s for writing\n", filename);
    return -1;
  }

  generate->set_file(fp);
  generate->set_bank(bank);

  for (n = 0; n < 65536; n++)
  {
    if (has_code[n] != 0)
    {
      if (start == -1) { start = n; }
      if (start != -1 && n == 0xffff) { convert(fp , code, start, n); }
    }
    else
    {
      if (start != -1)
      {
        convert(fp, code, start, n);
        start = -1;
      }
    }
  }

  fclose(fp);

  bank++;

  return 0;
}

int Convert6502::find_blocks(uint8_t *code, int address)
{
  uint8_t opcode;
  int offset;
  int pushed = 0;
  int n;

  while(1)
  {
    // If this part of the ROM has already been confirmed already.
    if ((has_code[address] & 1) == 1) { break; }

    offset = address - 0xc000;
    opcode = code[offset];

    if (offset < 0 || offset > 16384)
    {
      printf("Warning: offset out of range\n");
    }

    if (table_6502[opcode].len == 0)
    {
      printf("Unknown instruction at 0x%04x (0x%02x)\n", address, opcode);
      break;
    }

    code_length += table_6502[opcode].len;
    instruction_count += 1;

    for (n = 0; n < table_6502[opcode].len; n++)
    {
      has_code[address + n] |= 1;
    }

    if (table_6502[opcode].branch == 1)
    {
      branch_stack.push((address + 2) + ((int8_t)code[offset + 1]));
      pushed++;

      // If the instruction is 'bra', then leave.
      if (opcode == 0x80) { break; }
    }
    else if (table_6502[opcode].branch == 2)
    {
      branch_stack.push(code[offset + 1] | (code[offset + 2] << 8));
      pushed++;

      break;
    }
    else if (table_6502[opcode].branch == 2)
    {
      printf("Warning: Found 'jmp' with indeterminable address 0x%02x\n", opcode);
      break;
    }

    address += table_6502[opcode].len;
  }

  while(pushed > 0)
  {
    int address = branch_stack.pop();

    has_code[address] |= 2;

    find_blocks(code, address);
    pushed--;
  }

  return 0;
}

int Convert6502::convert(FILE *fp, uint8_t *code, int start, int end)
{
  int offset;
  int value;
  uint8_t opcode;

  printf("Convert block 0x%04x to 0x%04x\n", start, end);

  fprintf(fp, "  ;; Block 0x%04x to 0x%04x\n", start, end);

  while(start != end)
  {
    if ((has_code[start] & 2) != 0)
    {
      fprintf(fp, "label_%04x_%d:\n", start, bank);
    }

    offset = start - 0xc000;
    opcode = code[offset];

    switch(opcode)
    {
      case 0x00:
        fprintf(fp, "  ;; %04x: brk\n", start);
        generate->append_brk();
        start += 1;
        break;
      case 0x01:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ora (0x%02x, x)\n", start, value);
        generate->append_ora_x_indirect8(value);
        start += 2;
        break;
      case 0x04:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: tsb 0x%02x\n", start, value);
        generate->append_tsb_address8(value);
        start += 2;
        break;
      case 0x05:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ora 0x%02x\n", start, value);
        generate->append_ora_address8(value);
        start += 2;
        break;
      case 0x06:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: asl 0x%02x\n", start, value);
        generate->append_asl_address8(value);
        start += 2;
        break;
      case 0x07:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rmb0 0x%02x\n", start, value);
        generate->append_rmb0_address8(value);
        start += 2;
        break;
      case 0x08:
        fprintf(fp, "  ;; %04x: php\n", start);
        generate->append_php();
        start += 1;
        break;
      case 0x09:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ora #0x%02x\n", start, value);
        generate->append_ora_imm(value);
        start += 2;
        break;
      case 0x0a:
        fprintf(fp, "  ;; %04x: asl\n", start);
        generate->append_asl();
        start += 1;
        break;
      case 0x0c:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: tsb 0x%04x\n", start, value);
        generate->append_tsb_address16(value);
        start += 3;
        break;
      case 0x0d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: ora 0x%04x\n", start, value);
        generate->append_ora_address16(value);
        start += 3;
        break;
      case 0x0e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: asl 0x%04x\n", start, value);
        generate->append_asl_address16(value);
        start += 3;
        break;
      case 0x10:
        value = ((int8_t)code[offset + 1]);
        fprintf(fp, "  ;; %04x: bpl 0x%04x (offset=%d)\n", start, start + 2 + value, value);
        generate->append_bpl_branch(start + 2 + value);
        start += 2;
        break;
      case 0x11:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ora (0x%02x), y\n", start, value);
        generate->append_ora_indirect8_y(value);
        start += 2;
        break;
      case 0x12:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ora (0x%02x)\n", start, value);
        generate->append_ora_indirect8(value);
        start += 2;
        break;
      case 0x14:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: trb 0x%02x\n", start, value);
        generate->append_trb_address8(value);
        start += 2;
        break;
      case 0x15:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ora 0x%02x, x\n", start, value);
        generate->append_ora_indexed8_x(value);
        start += 2;
        break;
      case 0x16:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: asl 0x%02x, x\n", start, value);
        generate->append_asl_indexed8_x(value);
        start += 2;
        break;
      case 0x17:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rmb1 0x%02x\n", start, value);
        generate->append_rmb1_address8(value);
        start += 2;
        break;
      case 0x18:
        fprintf(fp, "  ;; %04x: clc\n", start);
        generate->append_clc();
        start += 1;
        break;
      case 0x19:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: ora 0x%04x, y\n", start, value);
        generate->append_ora_indexed16_y(value);
        start += 3;
        break;
      case 0x1a:
        fprintf(fp, "  ;; %04x: inc\n", start);
        generate->append_inc();
        start += 1;
        break;
      case 0x1c:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: trb 0x%04x\n", start, value);
        generate->append_trb_address16(value);
        start += 3;
        break;
      case 0x1d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: ora 0x%04x, x\n", start, value);
        generate->append_ora_indexed16_x(value);
        start += 3;
        break;
      case 0x1e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: asl 0x%04x, x\n", start, value);
        generate->append_asl_indexed16_x(value);
        start += 3;
        break;
      case 0x20:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: jsr 0x%04x\n", start, value);
        generate->append_jsr_address16(value);
        start += 3;
        break;
      case 0x21:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: and (0x%02x, x)\n", start, value);
        generate->append_and_x_indirect8(value);
        start += 2;
        break;
      case 0x24:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: bit 0x%02x\n", start, value);
        generate->append_bit_address8(value);
        start += 2;
        break;
      case 0x25:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: and 0x%02x\n", start, value);
        generate->append_and_address8(value);
        start += 2;
        break;
      case 0x26:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rol 0x%02x\n", start, value);
        generate->append_rol_address8(value);
        start += 2;
        break;
      case 0x27:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rmb2 0x%02x\n", start, value);
        generate->append_rmb2_address8(value);
        start += 2;
        break;
      case 0x28:
        fprintf(fp, "  ;; %04x: plp\n", start);
        generate->append_plp();
        start += 1;
        break;
      case 0x29:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: and #0x%02x\n", start, value);
        generate->append_and_imm(value);
        start += 2;
        break;
      case 0x2a:
        fprintf(fp, "  ;; %04x: rol\n", start);
        generate->append_rol();
        start += 1;
        break;
      case 0x2c:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: bit 0x%04x\n", start, value);
        generate->append_bit_address16(value);
        start += 3;
        break;
      case 0x2d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: and 0x%04x\n", start, value);
        generate->append_and_address16(value);
        start += 3;
        break;
      case 0x2e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: rol 0x%04x\n", start, value);
        generate->append_rol_address16(value);
        start += 3;
        break;
      case 0x30:
        value = ((int8_t)code[offset + 1]);
        fprintf(fp, "  ;; %04x: bmi 0x%04x (offset=%d)\n", start, start + 2 + value, value);
        generate->append_bmi_branch(start + 2 + value);
        start += 2;
        break;
      case 0x31:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: and (0x%02x), y\n", start, value);
        generate->append_and_indirect8_y(value);
        start += 2;
        break;
      case 0x32:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: and (0x%02x)\n", start, value);
        generate->append_and_indirect8(value);
        start += 2;
        break;
      case 0x34:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: bit 0x%02x, x\n", start, value);
        generate->append_bit_indexed8_x(value);
        start += 2;
        break;
      case 0x35:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: and 0x%02x, x\n", start, value);
        generate->append_and_indexed8_x(value);
        start += 2;
        break;
      case 0x36:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rol 0x%02x, x\n", start, value);
        generate->append_rol_indexed8_x(value);
        start += 2;
        break;
      case 0x37:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rmb3 0x%02x\n", start, value);
        generate->append_rmb3_address8(value);
        start += 2;
        break;
      case 0x38:
        fprintf(fp, "  ;; %04x: sec\n", start);
        generate->append_sec();
        start += 1;
        break;
      case 0x39:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: and 0x%04x, y\n", start, value);
        generate->append_and_indexed16_y(value);
        start += 3;
        break;
      case 0x3a:
        fprintf(fp, "  ;; %04x: dec\n", start);
        generate->append_dec();
        start += 1;
        break;
      case 0x3c:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: bit 0x%04x, x\n", start, value);
        generate->append_bit_indexed16_x(value);
        start += 3;
        break;
      case 0x3d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: and 0x%04x, x\n", start, value);
        generate->append_and_indexed16_x(value);
        start += 3;
        break;
      case 0x3e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: rol 0x%04x, x\n", start, value);
        generate->append_rol_indexed16_x(value);
        start += 3;
        break;
      case 0x40:
        fprintf(fp, "  ;; %04x: rti\n", start);
        generate->append_rti();
        start += 1;
        break;
      case 0x41:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: eor (0x%02x, x)\n", start, value);
        generate->append_eor_x_indirect8(value);
        start += 2;
        break;
      case 0x45:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: eor 0x%02x\n", start, value);
        generate->append_eor_address8(value);
        start += 2;
        break;
      case 0x46:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: lsr 0x%02x\n", start, value);
        generate->append_lsr_address8(value);
        start += 2;
        break;
      case 0x47:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rmb4 0x%02x\n", start, value);
        generate->append_rmb4_address8(value);
        start += 2;
        break;
      case 0x48:
        fprintf(fp, "  ;; %04x: pha\n", start);
        generate->append_pha();
        start += 1;
        break;
      case 0x49:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: eor #0x%02x\n", start, value);
        generate->append_eor_imm(value);
        start += 2;
        break;
      case 0x4a:
        fprintf(fp, "  ;; %04x: lsr\n", start);
        generate->append_lsr();
        start += 1;
        break;
      case 0x4c:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: jmp 0x%04x\n", start, value);
        generate->append_jmp_address16(value);
        start += 3;
        break;
      case 0x4d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: eor 0x%04x\n", start, value);
        generate->append_eor_address16(value);
        start += 3;
        break;
      case 0x4e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: lsr 0x%04x\n", start, value);
        generate->append_lsr_address16(value);
        start += 3;
        break;
      case 0x50:
        value = ((int8_t)code[offset + 1]);
        fprintf(fp, "  ;; %04x: bvc 0x%04x (offset=%d)\n", start, start + 2 + value, value);
        generate->append_bvc_branch(start + 2 + value);
        start += 2;
        break;
      case 0x51:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: eor (0x%02x), y\n", start, value);
        generate->append_eor_indirect8_y(value);
        start += 2;
        break;
      case 0x52:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: eor (0x%02x)\n", start, value);
        generate->append_eor_indirect8(value);
        start += 2;
        break;
      case 0x55:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: eor 0x%02x, x\n", start, value);
        generate->append_eor_indexed8_x(value);
        start += 2;
        break;
      case 0x56:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: lsr 0x%02x, x\n", start, value);
        generate->append_lsr_indexed8_x(value);
        start += 2;
        break;
      case 0x57:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rmb5 0x%02x\n", start, value);
        generate->append_rmb5_address8(value);
        start += 2;
        break;
      case 0x58:
        fprintf(fp, "  ;; %04x: cli\n", start);
        generate->append_cli();
        start += 1;
        break;
      case 0x59:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: eor 0x%04x, y\n", start, value);
        generate->append_eor_indexed16_y(value);
        start += 3;
        break;
      case 0x5a:
        fprintf(fp, "  ;; %04x: phy\n", start);
        generate->append_phy();
        start += 1;
        break;
      case 0x5d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: eor 0x%04x, x\n", start, value);
        generate->append_eor_indexed16_x(value);
        start += 3;
        break;
      case 0x5e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: lsr 0x%04x, x\n", start, value);
        generate->append_lsr_indexed16_x(value);
        start += 3;
        break;
      case 0x60:
        fprintf(fp, "  ;; %04x: rts\n", start);
        generate->append_rts();
        start += 1;
        break;
      case 0x61:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: adc (0x%02x, x)\n", start, value);
        generate->append_adc_x_indirect8(value);
        start += 2;
        break;
      case 0x64:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: stz 0x%02x\n", start, value);
        generate->append_stz_address8(value);
        start += 2;
        break;
      case 0x65:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: adc 0x%02x\n", start, value);
        generate->append_adc_address8(value);
        start += 2;
        break;
      case 0x66:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ror 0x%02x\n", start, value);
        generate->append_ror_address8(value);
        start += 2;
        break;
      case 0x67:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rmb6 0x%02x\n", start, value);
        generate->append_rmb6_address8(value);
        start += 2;
        break;
      case 0x68:
        fprintf(fp, "  ;; %04x: pla\n", start);
        generate->append_pla();
        start += 1;
        break;
      case 0x69:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: adc #0x%02x\n", start, value);
        generate->append_adc_imm(value);
        start += 2;
        break;
      case 0x6a:
        fprintf(fp, "  ;; %04x: ror\n", start);
        generate->append_ror();
        start += 1;
        break;
      case 0x6c:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: jmp (0x%04x)\n", start, value);
        generate->append_jmp_indirect16(value);
        start += 3;
        break;
      case 0x6d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: adc 0x%04x\n", start, value);
        generate->append_adc_address16(value);
        start += 3;
        break;
      case 0x6e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: ror 0x%04x\n", start, value);
        generate->append_ror_address16(value);
        start += 3;
        break;
      case 0x70:
        value = ((int8_t)code[offset + 1]);
        fprintf(fp, "  ;; %04x: bvs 0x%04x (offset=%d)\n", start, start + 2 + value, value);
        generate->append_bvs_branch(start + 2 + value);
        start += 2;
        break;
      case 0x71:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: adc (0x%02x), y\n", start, value);
        generate->append_adc_indirect8_y(value);
        start += 2;
        break;
      case 0x72:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: adc (0x%02x)\n", start, value);
        generate->append_adc_indirect8(value);
        start += 2;
        break;
      case 0x74:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: stz 0x%02x, x\n", start, value);
        generate->append_stz_indexed8_x(value);
        start += 2;
        break;
      case 0x75:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: adc 0x%02x, x\n", start, value);
        generate->append_adc_indexed8_x(value);
        start += 2;
        break;
      case 0x76:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ror 0x%02x, x\n", start, value);
        generate->append_ror_indexed8_x(value);
        start += 2;
        break;
      case 0x77:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: rmb7 0x%02x\n", start, value);
        generate->append_rmb7_address8(value);
        start += 2;
        break;
      case 0x78:
        fprintf(fp, "  ;; %04x: sei\n", start);
        generate->append_sei();
        start += 1;
        break;
      case 0x79:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: adc 0x%04x, y\n", start, value);
        generate->append_adc_indexed16_y(value);
        start += 3;
        break;
      case 0x7a:
        fprintf(fp, "  ;; %04x: ply\n", start);
        generate->append_ply();
        start += 1;
        break;
      case 0x7c:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: jmp (0x%04x, x)\n", start, value);
        generate->append_jmp_x_indirect16(value);
        start += 3;
        break;
      case 0x7d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: adc 0x%04x, x\n", start, value);
        generate->append_adc_indexed16_x(value);
        start += 3;
        break;
      case 0x7e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: ror 0x%04x, x\n", start, value);
        generate->append_ror_indexed16_x(value);
        start += 3;
        break;
      case 0x80:
        value = ((int8_t)code[offset + 1]);
        fprintf(fp, "  ;; %04x: bra 0x%04x (offset=%d)\n", start, start + 2 + value, value);
        generate->append_bra_branch(start + 2 + value);
        start += 2;
        break;
      case 0x81:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sta (0x%02x, x)\n", start, value);
        generate->append_sta_x_indirect8(value);
        start += 2;
        break;
      case 0x84:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sty 0x%02x\n", start, value);
        generate->append_sty_address8(value);
        start += 2;
        break;
      case 0x85:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sta 0x%02x\n", start, value);
        generate->append_sta_address8(value);
        start += 2;
        break;
      case 0x86:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: stx 0x%02x\n", start, value);
        generate->append_stx_address8(value);
        start += 2;
        break;
      case 0x87:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: smb0 0x%02x\n", start, value);
        generate->append_smb0_address8(value);
        start += 2;
        break;
      case 0x88:
        fprintf(fp, "  ;; %04x: dey\n", start);
        generate->append_dey();
        start += 1;
        break;
      case 0x89:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: bit #0x%02x\n", start, value);
        generate->append_bit_imm(value);
        start += 2;
        break;
      case 0x8a:
        fprintf(fp, "  ;; %04x: txa\n", start);
        generate->append_txa();
        start += 1;
        break;
      case 0x8c:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: sty 0x%04x\n", start, value);
        generate->append_sty_address16(value);
        start += 3;
        break;
      case 0x8d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: sta 0x%04x\n", start, value);
        generate->append_sta_address16(value);
        start += 3;
        break;
      case 0x8e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: stx 0x%04x\n", start, value);
        generate->append_stx_address16(value);
        start += 3;
        break;
      case 0x90:
        value = ((int8_t)code[offset + 1]);
        fprintf(fp, "  ;; %04x: bcc 0x%04x (offset=%d)\n", start, start + 2 + value, value);
        generate->append_bcc_branch(start + 2 + value);
        start += 2;
        break;
      case 0x91:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sta (0x%02x), y\n", start, value);
        generate->append_sta_indirect8_y(value);
        start += 2;
        break;
      case 0x92:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sta (0x%02x)\n", start, value);
        generate->append_sta_indirect8(value);
        start += 2;
        break;
      case 0x94:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sty 0x%02x, x\n", start, value);
        generate->append_sty_indexed8_x(value);
        start += 2;
        break;
      case 0x95:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sta 0x%02x, x\n", start, value);
        generate->append_sta_indexed8_x(value);
        start += 2;
        break;
      case 0x96:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: stx 0x%02x, y\n", start, value);
        generate->append_stx_indexed8_y(value);
        start += 2;
        break;
      case 0x97:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: smb1 0x%02x\n", start, value);
        generate->append_smb1_address8(value);
        start += 2;
        break;
      case 0x98:
        fprintf(fp, "  ;; %04x: tya\n", start);
        generate->append_tya();
        start += 1;
        break;
      case 0x99:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: sta 0x%04x, y\n", start, value);
        generate->append_sta_indexed16_y(value);
        start += 3;
        break;
      case 0x9a:
        fprintf(fp, "  ;; %04x: txs\n", start);
        generate->append_txs();
        start += 1;
        break;
      case 0x9c:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: stz 0x%04x\n", start, value);
        generate->append_stz_address16(value);
        start += 3;
        break;
      case 0x9d:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: sta 0x%04x, x\n", start, value);
        generate->append_sta_indexed16_x(value);
        start += 3;
        break;
      case 0x9e:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: stz 0x%04x, x\n", start, value);
        generate->append_stz_indexed16_x(value);
        start += 3;
        break;
      case 0xa0:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ldy #0x%02x\n", start, value);
        generate->append_ldy_imm(value);
        start += 2;
        break;
      case 0xa1:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: lda (0x%02x, x)\n", start, value);
        generate->append_lda_x_indirect8(value);
        start += 2;
        break;
      case 0xa2:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ldx #0x%02x\n", start, value);
        generate->append_ldx_imm(value);
        start += 2;
        break;
      case 0xa4:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ldy 0x%02x\n", start, value);
        generate->append_ldy_address8(value);
        start += 2;
        break;
      case 0xa5:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: lda 0x%02x\n", start, value);
        generate->append_lda_address8(value);
        start += 2;
        break;
      case 0xa6:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ldx 0x%02x\n", start, value);
        generate->append_ldx_address8(value);
        start += 2;
        break;
      case 0xa7:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: smb2 0x%02x\n", start, value);
        generate->append_smb2_address8(value);
        start += 2;
        break;
      case 0xa8:
        fprintf(fp, "  ;; %04x: tay\n", start);
        generate->append_tay();
        start += 1;
        break;
      case 0xa9:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: lda #0x%02x\n", start, value);
        generate->append_lda_imm(value);
        start += 2;
        break;
      case 0xaa:
        fprintf(fp, "  ;; %04x: tax\n", start);
        generate->append_tax();
        start += 1;
        break;
      case 0xac:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: ldy 0x%04x\n", start, value);
        generate->append_ldy_address16(value);
        start += 3;
        break;
      case 0xad:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: lda 0x%04x\n", start, value);
        generate->append_lda_address16(value);
        start += 3;
        break;
      case 0xae:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: ldx 0x%04x\n", start, value);
        generate->append_ldx_address16(value);
        start += 3;
        break;
      case 0xb0:
        value = ((int8_t)code[offset + 1]);
        fprintf(fp, "  ;; %04x: bcs 0x%04x (offset=%d)\n", start, start + 2 + value, value);
        generate->append_bcs_branch(start + 2 + value);
        start += 2;
        break;
      case 0xb1:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: lda (0x%02x), y\n", start, value);
        generate->append_lda_indirect8_y(value);
        start += 2;
        break;
      case 0xb2:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: lda (0x%02x)\n", start, value);
        generate->append_lda_indirect8(value);
        start += 2;
        break;
      case 0xb4:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ldy 0x%02x, x\n", start, value);
        generate->append_ldy_indexed8_x(value);
        start += 2;
        break;
      case 0xb5:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: lda 0x%02x, x\n", start, value);
        generate->append_lda_indexed8_x(value);
        start += 2;
        break;
      case 0xb6:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: ldx 0x%02x, y\n", start, value);
        generate->append_ldx_indexed8_y(value);
        start += 2;
        break;
      case 0xb7:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: smb3 0x%02x\n", start, value);
        generate->append_smb3_address8(value);
        start += 2;
        break;
      case 0xb8:
        fprintf(fp, "  ;; %04x: clv\n", start);
        generate->append_clv();
        start += 1;
        break;
      case 0xb9:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: lda 0x%04x, y\n", start, value);
        generate->append_lda_indexed16_y(value);
        start += 3;
        break;
      case 0xba:
        fprintf(fp, "  ;; %04x: tsx\n", start);
        generate->append_tsx();
        start += 1;
        break;
      case 0xbc:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: ldy 0x%04x, x\n", start, value);
        generate->append_ldy_indexed16_x(value);
        start += 3;
        break;
      case 0xbd:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: lda 0x%04x, x\n", start, value);
        generate->append_lda_indexed16_x(value);
        start += 3;
        break;
      case 0xbe:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: ldx 0x%04x, y\n", start, value);
        generate->append_ldx_indexed16_y(value);
        start += 3;
        break;
      case 0xc0:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cpy #0x%02x\n", start, value);
        generate->append_cpy_imm(value);
        start += 2;
        break;
      case 0xc1:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cmp (0x%02x, x)\n", start, value);
        generate->append_cmp_x_indirect8(value);
        start += 2;
        break;
      case 0xc4:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cpy 0x%02x\n", start, value);
        generate->append_cpy_address8(value);
        start += 2;
        break;
      case 0xc5:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cmp 0x%02x\n", start, value);
        generate->append_cmp_address8(value);
        start += 2;
        break;
      case 0xc6:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: dec 0x%02x\n", start, value);
        generate->append_dec_address8(value);
        start += 2;
        break;
      case 0xc7:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: smb4 0x%02x\n", start, value);
        generate->append_smb4_address8(value);
        start += 2;
        break;
      case 0xc8:
        fprintf(fp, "  ;; %04x: iny\n", start);
        generate->append_iny();
        start += 1;
        break;
      case 0xc9:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cmp #0x%02x\n", start, value);
        generate->append_cmp_imm(value);
        start += 2;
        break;
      case 0xca:
        fprintf(fp, "  ;; %04x: dex\n", start);
        generate->append_dex();
        start += 1;
        break;
      case 0xcb:
        fprintf(fp, "  ;; %04x: wai\n", start);
        generate->append_wai();
        start += 1;
        break;
      case 0xcc:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: cpy 0x%04x\n", start, value);
        generate->append_cpy_address16(value);
        start += 3;
        break;
      case 0xcd:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: cmp 0x%04x\n", start, value);
        generate->append_cmp_address16(value);
        start += 3;
        break;
      case 0xce:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: dec 0x%04x\n", start, value);
        generate->append_dec_address16(value);
        start += 3;
        break;
      case 0xd0:
        value = ((int8_t)code[offset + 1]);
        fprintf(fp, "  ;; %04x: bne 0x%04x (offset=%d)\n", start, start + 2 + value, value);
        generate->append_bne_branch(start + 2 + value);
        start += 2;
        break;
      case 0xd1:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cmp (0x%02x), y\n", start, value);
        generate->append_cmp_indirect8_y(value);
        start += 2;
        break;
      case 0xd2:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cmp (0x%02x)\n", start, value);
        generate->append_cmp_indirect8(value);
        start += 2;
        break;
      case 0xd5:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cmp 0x%02x, x\n", start, value);
        generate->append_cmp_indexed8_x(value);
        start += 2;
        break;
      case 0xd6:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: dec 0x%02x, x\n", start, value);
        generate->append_dec_indexed8_x(value);
        start += 2;
        break;
      case 0xd7:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: smb5 0x%02x\n", start, value);
        generate->append_smb5_address8(value);
        start += 2;
        break;
      case 0xd8:
        fprintf(fp, "  ;; %04x: cld\n", start);
        generate->append_cld();
        start += 1;
        break;
      case 0xd9:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: cmp 0x%04x, y\n", start, value);
        generate->append_cmp_indexed16_y(value);
        start += 3;
        break;
      case 0xda:
        fprintf(fp, "  ;; %04x: phx\n", start);
        generate->append_phx();
        start += 1;
        break;
      case 0xdb:
        fprintf(fp, "  ;; %04x: stp\n", start);
        generate->append_stp();
        start += 1;
        break;
      case 0xdd:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: cmp 0x%04x, x\n", start, value);
        generate->append_cmp_indexed16_x(value);
        start += 3;
        break;
      case 0xde:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: dec 0x%04x, x\n", start, value);
        generate->append_dec_indexed16_x(value);
        start += 3;
        break;
      case 0xe0:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cpx #0x%02x\n", start, value);
        generate->append_cpx_imm(value);
        start += 2;
        break;
      case 0xe1:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sbc (0x%02x, x)\n", start, value);
        generate->append_sbc_x_indirect8(value);
        start += 2;
        break;
      case 0xe4:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: cpx 0x%02x\n", start, value);
        generate->append_cpx_address8(value);
        start += 2;
        break;
      case 0xe5:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sbc 0x%02x\n", start, value);
        generate->append_sbc_address8(value);
        start += 2;
        break;
      case 0xe6:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: inc 0x%02x\n", start, value);
        generate->append_inc_address8(value);
        start += 2;
        break;
      case 0xe7:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: smb6 0x%02x\n", start, value);
        generate->append_smb6_address8(value);
        start += 2;
        break;
      case 0xe8:
        fprintf(fp, "  ;; %04x: inx\n", start);
        generate->append_inx();
        start += 1;
        break;
      case 0xe9:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sbc #0x%02x\n", start, value);
        generate->append_sbc_imm(value);
        start += 2;
        break;
      case 0xea:
        fprintf(fp, "  ;; %04x: nop\n", start);
        generate->append_nop();
        start += 1;
        break;
      case 0xec:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: cpx 0x%04x\n", start, value);
        generate->append_cpx_address16(value);
        start += 3;
        break;
      case 0xed:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: sbc 0x%04x\n", start, value);
        generate->append_sbc_address16(value);
        start += 3;
        break;
      case 0xee:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: inc 0x%04x\n", start, value);
        generate->append_inc_address16(value);
        start += 3;
        break;
      case 0xf0:
        value = ((int8_t)code[offset + 1]);
        fprintf(fp, "  ;; %04x: beq 0x%04x (offset=%d)\n", start, start + 2 + value, value);
        generate->append_beq_branch(start + 2 + value);
        start += 2;
        break;
      case 0xf1:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sbc (0x%02x), y\n", start, value);
        generate->append_sbc_indirect8_y(value);
        start += 2;
        break;
      case 0xf2:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sbc (0x%02x)\n", start, value);
        generate->append_sbc_indirect8(value);
        start += 2;
        break;
      case 0xf5:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: sbc 0x%02x, x\n", start, value);
        generate->append_sbc_indexed8_x(value);
        start += 2;
        break;
      case 0xf6:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: inc 0x%02x, x\n", start, value);
        generate->append_inc_indexed8_x(value);
        start += 2;
        break;
      case 0xf7:
        value = code[offset + 1];
        fprintf(fp, "  ;; %04x: smb7 0x%02x\n", start, value);
        generate->append_smb7_address8(value);
        start += 2;
        break;
      case 0xf8:
        fprintf(fp, "  ;; %04x: sed\n", start);
        generate->append_sed();
        start += 1;
        break;
      case 0xf9:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: sbc 0x%04x, y\n", start, value);
        generate->append_sbc_indexed16_y(value);
        start += 3;
        break;
      case 0xfa:
        fprintf(fp, "  ;; %04x: plx\n", start);
        generate->append_plx();
        start += 1;
        break;
      case 0xfd:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: sbc 0x%04x, x\n", start, value);
        generate->append_sbc_indexed16_x(value);
        start += 3;
        break;
      case 0xfe:
        value = code[offset + 1] | (code[offset + 2] << 8);
        fprintf(fp, "  ;; %04x: inc 0x%04x, x\n", start, value);
        generate->append_inc_indexed16_x(value);
        start += 3;
        break;
      default:
        printf("Unknown opcode 0x%04x: 0x%02x\n", start, opcode);
        start++;
        break;
    }
  }

  return 0;
}

