/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "Generate68000.h"

Generate68000::Generate68000() :
  fp(NULL)
{
}

Generate68000::~Generate68000()
{
}

// ADD START
int Generate68000::append_brk()
{
  return -1;
}

int Generate68000::append_ora_x_indirect8(int value)
{
  return -1;
}

int Generate68000::append_tsb_address8(int value)
{
  return -1;
}

int Generate68000::append_ora_address8(int value)
{
  return -1;
}

int Generate68000::append_asl_address8(int value)
{
  return -1;
}

int Generate68000::append_rmb0_address8(int value)
{
  return -1;
}

int Generate68000::append_php()
{
  return -1;
}

int Generate68000::append_ora_imm(int value)
{
  return -1;
}

int Generate68000::append_asl()
{
  fprintf(fp, "  asl.b #1, d0\n");
  return 0;
}

int Generate68000::append_tsb_address16(int value)
{
  return -1;
}

int Generate68000::append_ora_address16(int value)
{
  return -1;
}

int Generate68000::append_asl_address16(int value)
{
  return -1;
}

int Generate68000::append_bpl_branch(int value)
{
  fprintf(fp, "  bpl label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_ora_indirect8_y(int value)
{
  return -1;
}

int Generate68000::append_ora_indirect8(int value)
{
  return -1;
}

int Generate68000::append_trb_address8(int value)
{
  return -1;
}

int Generate68000::append_ora_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_asl_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_rmb1_address8(int value)
{
  return -1;
}

int Generate68000::append_clc()
{
  return -1;
}

int Generate68000::append_ora_indexed16_y(int value)
{
  return -1;
}

int Generate68000::append_inc()
{
  fprintf(fp, "  addq.b #1, d0\n");
  return 0;
}

int Generate68000::append_trb_address16(int value)
{
  return -1;
}

int Generate68000::append_ora_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_asl_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_jsr_address16(int value)
{
  fprintf(fp, "  jsr label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_and_x_indirect8(int value)
{
  return -1;
}

int Generate68000::append_bit_address8(int value)
{
  return -1;
}

int Generate68000::append_and_address8(int value)
{
  return -1;
}

int Generate68000::append_rol_address8(int value)
{
  return -1;
}

int Generate68000::append_rmb2_address8(int value)
{
  return -1;
}

int Generate68000::append_plp()
{
  return -1;
}

int Generate68000::append_and_imm(int value)
{
  return -1;
}

int Generate68000::append_rol()
{
  fprintf(fp, "  rol.b #1, d0\n");
  return 0;
}

int Generate68000::append_bit_address16(int value)
{
  return -1;
}

int Generate68000::append_and_address16(int value)
{
  return -1;
}

int Generate68000::append_rol_address16(int value)
{
  return -1;
}

int Generate68000::append_bmi_branch(int value)
{
  fprintf(fp, "  bmi label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_and_indirect8_y(int value)
{
  return -1;
}

int Generate68000::append_and_indirect8(int value)
{
  return -1;
}

int Generate68000::append_bit_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_and_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_rol_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_rmb3_address8(int value)
{
  return -1;
}

int Generate68000::append_sec()
{
  return -1;
}

int Generate68000::append_and_indexed16_y(int value)
{
  return -1;
}

int Generate68000::append_dec()
{
  fprintf(fp, "  subq.b #1, d0\n");
  return 0;
}

int Generate68000::append_bit_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_and_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_rol_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_rti()
{
  return -1;
}

int Generate68000::append_eor_x_indirect8(int value)
{
  return -1;
}

int Generate68000::append_eor_address8(int value)
{
  return -1;
}

int Generate68000::append_lsr_address8(int value)
{
  return -1;
}

int Generate68000::append_rmb4_address8(int value)
{
  return -1;
}

int Generate68000::append_pha()
{
  fprintf(fp, "  movea.l d0, -(a7)\n");
  return 0;
}

int Generate68000::append_eor_imm(int value)
{
  return -1;
}

int Generate68000::append_lsr()
{
  fprintf(fp, "  lsr.b #1, d0\n");
  return 0;
}

int Generate68000::append_jmp_address16(int value)
{
  fprintf(fp, "  jmp label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_eor_address16(int value)
{
  return -1;
}

int Generate68000::append_lsr_address16(int value)
{
  return -1;
}

int Generate68000::append_bvc_branch(int value)
{
  fprintf(fp, "  bvc label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_eor_indirect8_y(int value)
{
  return -1;
}

int Generate68000::append_eor_indirect8(int value)
{
  return -1;
}

int Generate68000::append_eor_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_lsr_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_rmb5_address8(int value)
{
  return -1;
}

int Generate68000::append_cli()
{
  return -1;
}

int Generate68000::append_eor_indexed16_y(int value)
{
  return -1;
}

int Generate68000::append_phy()
{
  fprintf(fp, "  movea.l a1, -(a7)\n");
  return 0;
}

int Generate68000::append_eor_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_lsr_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_rts()
{
  fprintf(fp, "  rts\n");
  return 0;
}

int Generate68000::append_adc_x_indirect8(int value)
{
  return -1;
}

int Generate68000::append_stz_address8(int value)
{
  return -1;
}

int Generate68000::append_adc_address8(int value)
{
  return -1;
}

int Generate68000::append_ror_address8(int value)
{
  return -1;
}

int Generate68000::append_rmb6_address8(int value)
{
  return -1;
}

int Generate68000::append_pla()
{
  fprintf(fp, "  movea.l -(a7), d0\n");
  return 0;
}

int Generate68000::append_adc_imm(int value)
{
  return -1;
}

int Generate68000::append_ror()
{
  return -1;
}

int Generate68000::append_jmp_indirect16(int value)
{
  return -1;
}

int Generate68000::append_adc_address16(int value)
{
  return -1;
}

int Generate68000::append_ror_address16(int value)
{
  return -1;
}

int Generate68000::append_bvs_branch(int value)
{
  fprintf(fp, "  bvs label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_adc_indirect8_y(int value)
{
  return -1;
}

int Generate68000::append_adc_indirect8(int value)
{
  return -1;
}

int Generate68000::append_stz_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_adc_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_ror_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_rmb7_address8(int value)
{
  return -1;
}

int Generate68000::append_sei()
{
  return -1;
}

int Generate68000::append_adc_indexed16_y(int value)
{
  return -1;
}

int Generate68000::append_ply()
{
  fprintf(fp, "  movea.l -(a7), a1\n");
  return 0;
}

int Generate68000::append_jmp_x_indirect16(int value)
{
  return -1;
}

int Generate68000::append_adc_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_ror_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_bra_branch(int value)
{
  fprintf(fp, "  bra label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_sta_x_indirect8(int value)
{
  return -1;
}

int Generate68000::append_sty_address8(int value)
{
  fprintf(fp, "  movea a1, (0x%02x,a2)\n", value & 0xff);
  return 0;
}

int Generate68000::append_sta_address8(int value)
{
  fprintf(fp, "  move.b d0, (0x%02x,a2)\n", value & 0xff);
  return 0;
}

int Generate68000::append_stx_address8(int value)
{
  fprintf(fp, "  movea a0, (0x%02x,a2)\n", value & 0xff);
  return 0;
}

int Generate68000::append_smb0_address8(int value)
{
  return -1;
}

int Generate68000::append_dey()
{
  fprintf(fp, "  subq.b #1, a1\n");
  return 0;
}

int Generate68000::append_bit_imm(int value)
{
  return -1;
}

int Generate68000::append_txa()
{
  fprintf(fp, "  move.b a0, d0\n");
  return 0;
}

int Generate68000::append_sty_address16(int value)
{
  fprintf(fp, "  movea a1, (0x%04x,a2)\n", value);
  return 0;
}

int Generate68000::append_sta_address16(int value)
{
  fprintf(fp, "  move.b d0, (0x%04x,a2)\n", value);
  return 0;
}

int Generate68000::append_stx_address16(int value)
{
  fprintf(fp, "  movea a0, (0x%04x,a2)\n", value);
  return 0;
}

int Generate68000::append_bcc_branch(int value)
{
  fprintf(fp, "  bcc label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_sta_indirect8_y(int value)
{
  return -1;
}

int Generate68000::append_sta_indirect8(int value)
{
  return -1;
}

int Generate68000::append_sty_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_sta_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_stx_indexed8_y(int value)
{
  return -1;
}

int Generate68000::append_smb1_address8(int value)
{
  return -1;
}

int Generate68000::append_tya()
{
  fprintf(fp, "  move.b a1, d0\n");
  return 0;
}

int Generate68000::append_sta_indexed16_y(int value)
{
  return -1;
}

int Generate68000::append_txs()
{
  return -1;
}

int Generate68000::append_stz_address16(int value)
{
  return -1;
}

int Generate68000::append_sta_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_stz_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_ldy_imm(int value)
{
  fprintf(fp, "  movea #0x%02x, a1\n", value & 0xff);
  return 0;
}

int Generate68000::append_lda_x_indirect8(int value)
{
  return -1;
}

int Generate68000::append_ldx_imm(int value)
{
  fprintf(fp, "  movea #0x%02x, a0\n", value & 0xff);
  return 0;
}

int Generate68000::append_ldy_address8(int value)
{
  fprintf(fp, "  movea (0x%02x,a2), a1\n", value & 0xff);
  return 0;
}

int Generate68000::append_lda_address8(int value)
{
  fprintf(fp, "  move.b (0x%02x,a2), d0\n", value & 0xff);
  return 0;
}

int Generate68000::append_ldx_address8(int value)
{
  fprintf(fp, "  movea (0x%02x,a2), a0\n", value & 0xff);
  return 0;
}

int Generate68000::append_smb2_address8(int value)
{
  return -1;
}

int Generate68000::append_tay()
{
  fprintf(fp, "  move.b d0, a1\n");
  return 0;
}

int Generate68000::append_lda_imm(int value)
{
  fprintf(fp, "  move.b #0x%02x, d0\n", value & 0xff);
  return 0;
}

int Generate68000::append_tax()
{
  fprintf(fp, "  move.b d0, a0\n");
  return 0;
}

int Generate68000::append_ldy_address16(int value)
{
  fprintf(fp, "  movea (0x%04x,a2), a1\n", value);
  return 0;
}

int Generate68000::append_lda_address16(int value)
{
  fprintf(fp, "  move.b (0x%04x,a2), d0\n", value);
  return 0;
}

int Generate68000::append_ldx_address16(int value)
{
  fprintf(fp, "  movea (0x%04x,a2), a0\n", value);
  return 0;
}

int Generate68000::append_bcs_branch(int value)
{
  fprintf(fp, "  bcs label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_lda_indirect8_y(int value)
{
  return -1;
}

int Generate68000::append_lda_indirect8(int value)
{
  return -1;
}

int Generate68000::append_ldy_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_lda_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_ldx_indexed8_y(int value)
{
  return -1;
}

int Generate68000::append_smb3_address8(int value)
{
  return -1;
}

int Generate68000::append_clv()
{
  return -1;
}

int Generate68000::append_lda_indexed16_y(int value)
{
  return -1;
}

int Generate68000::append_tsx()
{
  return -1;
}

int Generate68000::append_ldy_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_lda_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_ldx_indexed16_y(int value)
{
  return -1;
}

int Generate68000::append_cpy_imm(int value)
{
  return -1;
}

int Generate68000::append_cmp_x_indirect8(int value)
{
  return -1;
}

int Generate68000::append_cpy_address8(int value)
{
  return -1;
}

int Generate68000::append_cmp_address8(int value)
{
  return -1;
}

int Generate68000::append_dec_address8(int value)
{
  return -1;
}

int Generate68000::append_smb4_address8(int value)
{
  return -1;
}

int Generate68000::append_iny()
{
  fprintf(fp, "  addq.b #1, a1\n");
  return 0;
}

int Generate68000::append_cmp_imm(int value)
{
  return -1;
}

int Generate68000::append_dex()
{
  fprintf(fp, "  subq.b #1, a0\n");
  return 0;
}

int Generate68000::append_wai()
{
  return -1;
}

int Generate68000::append_cpy_address16(int value)
{
  return -1;
}

int Generate68000::append_cmp_address16(int value)
{
  return -1;
}

int Generate68000::append_dec_address16(int value)
{
  return -1;
}

int Generate68000::append_bne_branch(int value)
{
  fprintf(fp, "  bne label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_cmp_indirect8_y(int value)
{
  return -1;
}

int Generate68000::append_cmp_indirect8(int value)
{
  return -1;
}

int Generate68000::append_cmp_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_dec_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_smb5_address8(int value)
{
  return -1;
}

int Generate68000::append_cld()
{
  return -1;
}

int Generate68000::append_cmp_indexed16_y(int value)
{
  return -1;
}

int Generate68000::append_phx()
{
  fprintf(fp, "  movea.l a0, -(a7)\n");
  return 0;
}

int Generate68000::append_stp()
{
  return -1;
}

int Generate68000::append_cmp_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_dec_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_cpx_imm(int value)
{
  return -1;
}

int Generate68000::append_sbc_x_indirect8(int value)
{
  return -1;
}

int Generate68000::append_cpx_address8(int value)
{
  return -1;
}

int Generate68000::append_sbc_address8(int value)
{
  return -1;
}

int Generate68000::append_inc_address8(int value)
{
  return -1;
}

int Generate68000::append_smb6_address8(int value)
{
  return -1;
}

int Generate68000::append_inx()
{
  fprintf(fp, "  addq.b #1, a0\n");
  return 0;
}

int Generate68000::append_sbc_imm(int value)
{
  return -1;
}

int Generate68000::append_nop()
{
  fprintf(fp, "  nop\n");
  return 0;
}

int Generate68000::append_cpx_address16(int value)
{
  return -1;
}

int Generate68000::append_sbc_address16(int value)
{
  return -1;
}

int Generate68000::append_inc_address16(int value)
{
  return -1;
}

int Generate68000::append_beq_branch(int value)
{
  fprintf(fp, "  beq label_%04x_%d\n", value, bank);
  return 0;
}

int Generate68000::append_sbc_indirect8_y(int value)
{
  return -1;
}

int Generate68000::append_sbc_indirect8(int value)
{
  return -1;
}

int Generate68000::append_sbc_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_inc_indexed8_x(int value)
{
  return -1;
}

int Generate68000::append_smb7_address8(int value)
{
  return -1;
}

int Generate68000::append_sed()
{
  return -1;
}

int Generate68000::append_sbc_indexed16_y(int value)
{
  return -1;
}

int Generate68000::append_plx()
{
  fprintf(fp, "  movea.l -(a7), a0\n");
  return 0;
}

int Generate68000::append_sbc_indexed16_x(int value)
{
  return -1;
}

int Generate68000::append_inc_indexed16_x(int value)
{
  return -1;
}

// ADD STOP

#if 0
int Generate68000::open(const char *filename)
{
  fp = fopen(filename, "wb");

  return (fp == NULL) ? -1 : 0;
}

void Generate68000::close()
{
  if (fp != NULL) { fclose(fp); }
  fp = NULL;
}
#endif

