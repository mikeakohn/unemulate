/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#ifndef CONVERT_6502
#define CONVERT_6502

#include "Generate68000.h"

class Convert6502
{
public:
  Convert6502();
  ~Convert6502();

  int process(uint8_t *code);

private:
  struct BranchStack
  {
    BranchStack() : next(0) { }
    void push(int address) { stack[next++] = address; }
    uint16_t pop() { return stack[--next]; }
    int next;
    uint16_t stack[65536];
  };

  int find_blocks(uint8_t *code, int address);
  int convert(FILE *fp, uint8_t *code, int start, int end);

  int instruction_count;
  int code_length;
  int bank;
  Generate68000 *generate;
  uint8_t has_code[65536];
  BranchStack branch_stack;
};

#endif

