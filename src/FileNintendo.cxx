/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "FileNintendo.h"

#define PRG_BANK_SIZE 16384
#define CHR_BANK_SIZE 8192

FileNintendo::FileNintendo() :
  fp(NULL),
  data(NULL),
  data_length(0),
  prg_banks(0),
  chr_banks(0)
{
}

FileNintendo::~FileNintendo()
{
  if (fp == NULL) { fclose(fp); }
  free(data);
}

int FileNintendo::open(const char *filename)
{
  struct stat statbuf;

  fp = fopen(filename, "rb");

  if (fp == NULL)
  {
    printf("File not found %s\n", filename);
    return -1;
  }

  if (stat(filename, &statbuf) != 0)
  {
    printf("Cannot stat file %s\n", filename);
    fclose(fp);
    return -1;
  }

  data_length = statbuf.st_size;

  printf("File size=%d\n", data_length);

  data = (uint8_t *)malloc(data_length);

  if (fread(data, data_length, 1, fp) != 1)
  {
    printf("Couldn't read whole file\n");
    return -1;
  }

  if (data[0] != 'N' || data[1] != 'E' || data[2] != 'S' || data[3] != 0x1a)
  {
    printf("Not an NES file format\n");
    return -1;
  }

  prg_banks = data[4];
  chr_banks = data[5];

  printf("prg_banks: %d\n", prg_banks);
  printf("chr_banks: %d\n", chr_banks);

  int n;
  int ptr = 16;

  for (n = 0; n < prg_banks; n++)
  {
    int offset = ptr + PRG_BANK_SIZE;

    printf("PRG %04x\n", ptr);
    printf("PRG %04x: %02x %02x\n", offset - 4, data[offset - 4], data[offset - 3]);
    printf("PRG %04x: %02x %02x\n", offset - 2, data[offset - 2], data[offset - 1]);

    ptr = ptr + PRG_BANK_SIZE;
  }

  for (n = 0; n < chr_banks; n++)
  {
    printf("CHR %04x\n", ptr);

    ptr = ptr + CHR_BANK_SIZE;
  }

  printf("%d/%d\n", ptr, data_length);

  return 0;
}

uint8_t *FileNintendo::get_prg(int bank)
{
  if (bank >= prg_banks) { return NULL; }

  return data + 16 + (bank * PRG_BANK_SIZE);
}

uint8_t *FileNintendo::get_chr(int bank)
{
  if (bank >= chr_banks) { return NULL; }

  return data + 16 + (prg_banks * PRG_BANK_SIZE) + (bank * CHR_BANK_SIZE);
}

