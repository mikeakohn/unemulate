/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "Nintendo.h"
#include "Convert6502.h"

Nintendo::Nintendo()
{
}

Nintendo::~Nintendo()
{
}

int Nintendo::parse(FileNintendo *file)
{
  uint8_t *code;
  int bank = 0;
  Convert6502 *convert;

  convert = new Convert6502();

  while(1)
  {
    code = file->get_prg(bank++);
    if (code == NULL) { break; }

    convert->process(code);
  }

  delete convert;

  return 0;
}

