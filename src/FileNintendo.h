/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#ifndef FILE_NINTENDO
#define FILE_NINTENDO

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

class FileNintendo
{
public:
  FileNintendo();
  ~FileNintendo();
  int open(const char *filename);
  uint8_t *get_prg(int bank);
  uint8_t *get_chr(int bank);

private:
  FILE *fp;
  uint8_t *data;
  int data_length;
  int prg_banks;
  int chr_banks;
};

#endif

