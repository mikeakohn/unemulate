/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#ifndef GENERATE_68000
#define GENERATE_68000

class Generate68000
{
public:
  Generate68000();
  ~Generate68000();

  void set_file(FILE *fp) { this->fp = fp; }
  void set_bank(int bank) { this->bank = bank; }
  //int open(const char *filename);
  //void close();

  int append_brk();
  int append_ora_x_indirect8(int value);
  int append_tsb_address8(int value);
  int append_ora_address8(int value);
  int append_asl_address8(int value);
  int append_rmb0_address8(int value);
  int append_php();
  int append_ora_imm(int value);
  int append_asl();
  int append_tsb_address16(int value);
  int append_ora_address16(int value);
  int append_asl_address16(int value);
  int append_bpl_branch(int value);
  int append_ora_indirect8_y(int value);
  int append_ora_indirect8(int value);
  int append_trb_address8(int value);
  int append_ora_indexed8_x(int value);
  int append_asl_indexed8_x(int value);
  int append_rmb1_address8(int value);
  int append_clc();
  int append_ora_indexed16_y(int value);
  int append_inc();
  int append_trb_address16(int value);
  int append_ora_indexed16_x(int value);
  int append_asl_indexed16_x(int value);
  int append_jsr_address16(int value);
  int append_and_x_indirect8(int value);
  int append_bit_address8(int value);
  int append_and_address8(int value);
  int append_rol_address8(int value);
  int append_rmb2_address8(int value);
  int append_plp();
  int append_and_imm(int value);
  int append_rol();
  int append_bit_address16(int value);
  int append_and_address16(int value);
  int append_rol_address16(int value);
  int append_bmi_branch(int value);
  int append_and_indirect8_y(int value);
  int append_and_indirect8(int value);
  int append_bit_indexed8_x(int value);
  int append_and_indexed8_x(int value);
  int append_rol_indexed8_x(int value);
  int append_rmb3_address8(int value);
  int append_sec();
  int append_and_indexed16_y(int value);
  int append_dec();
  int append_bit_indexed16_x(int value);
  int append_and_indexed16_x(int value);
  int append_rol_indexed16_x(int value);
  int append_rti();
  int append_eor_x_indirect8(int value);
  int append_eor_address8(int value);
  int append_lsr_address8(int value);
  int append_rmb4_address8(int value);
  int append_pha();
  int append_eor_imm(int value);
  int append_lsr();
  int append_jmp_address16(int value);
  int append_eor_address16(int value);
  int append_lsr_address16(int value);
  int append_bvc_branch(int value);
  int append_eor_indirect8_y(int value);
  int append_eor_indirect8(int value);
  int append_eor_indexed8_x(int value);
  int append_lsr_indexed8_x(int value);
  int append_rmb5_address8(int value);
  int append_cli();
  int append_eor_indexed16_y(int value);
  int append_phy();
  int append_eor_indexed16_x(int value);
  int append_lsr_indexed16_x(int value);
  int append_rts();
  int append_adc_x_indirect8(int value);
  int append_stz_address8(int value);
  int append_adc_address8(int value);
  int append_ror_address8(int value);
  int append_rmb6_address8(int value);
  int append_pla();
  int append_adc_imm(int value);
  int append_ror();
  int append_jmp_indirect16(int value);
  int append_adc_address16(int value);
  int append_ror_address16(int value);
  int append_bvs_branch(int value);
  int append_adc_indirect8_y(int value);
  int append_adc_indirect8(int value);
  int append_stz_indexed8_x(int value);
  int append_adc_indexed8_x(int value);
  int append_ror_indexed8_x(int value);
  int append_rmb7_address8(int value);
  int append_sei();
  int append_adc_indexed16_y(int value);
  int append_ply();
  int append_jmp_x_indirect16(int value);
  int append_adc_indexed16_x(int value);
  int append_ror_indexed16_x(int value);
  int append_bra_branch(int value);
  int append_sta_x_indirect8(int value);
  int append_sty_address8(int value);
  int append_sta_address8(int value);
  int append_stx_address8(int value);
  int append_smb0_address8(int value);
  int append_dey();
  int append_bit_imm(int value);
  int append_txa();
  int append_sty_address16(int value);
  int append_sta_address16(int value);
  int append_stx_address16(int value);
  int append_bcc_branch(int value);
  int append_sta_indirect8_y(int value);
  int append_sta_indirect8(int value);
  int append_sty_indexed8_x(int value);
  int append_sta_indexed8_x(int value);
  int append_stx_indexed8_y(int value);
  int append_smb1_address8(int value);
  int append_tya();
  int append_sta_indexed16_y(int value);
  int append_txs();
  int append_stz_address16(int value);
  int append_sta_indexed16_x(int value);
  int append_stz_indexed16_x(int value);
  int append_ldy_imm(int value);
  int append_lda_x_indirect8(int value);
  int append_ldx_imm(int value);
  int append_ldy_address8(int value);
  int append_lda_address8(int value);
  int append_ldx_address8(int value);
  int append_smb2_address8(int value);
  int append_tay();
  int append_lda_imm(int value);
  int append_tax();
  int append_ldy_address16(int value);
  int append_lda_address16(int value);
  int append_ldx_address16(int value);
  int append_bcs_branch(int value);
  int append_lda_indirect8_y(int value);
  int append_lda_indirect8(int value);
  int append_ldy_indexed8_x(int value);
  int append_lda_indexed8_x(int value);
  int append_ldx_indexed8_y(int value);
  int append_smb3_address8(int value);
  int append_clv();
  int append_lda_indexed16_y(int value);
  int append_tsx();
  int append_ldy_indexed16_x(int value);
  int append_lda_indexed16_x(int value);
  int append_ldx_indexed16_y(int value);
  int append_cpy_imm(int value);
  int append_cmp_x_indirect8(int value);
  int append_cpy_address8(int value);
  int append_cmp_address8(int value);
  int append_dec_address8(int value);
  int append_smb4_address8(int value);
  int append_iny();
  int append_cmp_imm(int value);
  int append_dex();
  int append_wai();
  int append_cpy_address16(int value);
  int append_cmp_address16(int value);
  int append_dec_address16(int value);
  int append_bne_branch(int value);
  int append_cmp_indirect8_y(int value);
  int append_cmp_indirect8(int value);
  int append_cmp_indexed8_x(int value);
  int append_dec_indexed8_x(int value);
  int append_smb5_address8(int value);
  int append_cld();
  int append_cmp_indexed16_y(int value);
  int append_phx();
  int append_stp();
  int append_cmp_indexed16_x(int value);
  int append_dec_indexed16_x(int value);
  int append_cpx_imm(int value);
  int append_sbc_x_indirect8(int value);
  int append_cpx_address8(int value);
  int append_sbc_address8(int value);
  int append_inc_address8(int value);
  int append_smb6_address8(int value);
  int append_inx();
  int append_sbc_imm(int value);
  int append_nop();
  int append_cpx_address16(int value);
  int append_sbc_address16(int value);
  int append_inc_address16(int value);
  int append_beq_branch(int value);
  int append_sbc_indirect8_y(int value);
  int append_sbc_indirect8(int value);
  int append_sbc_indexed8_x(int value);
  int append_inc_indexed8_x(int value);
  int append_smb7_address8(int value);
  int append_sed();
  int append_sbc_indexed16_y(int value);
  int append_plx();
  int append_sbc_indexed16_x(int value);
  int append_inc_indexed16_x(int value);

private:
  FILE *fp;
  int bank;
};

#endif

