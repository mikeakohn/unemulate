/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "FileNintendo.h"
#include "Nintendo.h"

int main(int argc, char *argv[])
{
  FileNintendo *file;

  file = new FileNintendo();

  if (file->open(argv[1]) == 0)
  {
    Nintendo *nintendo = new Nintendo();
    nintendo->parse(file);
    delete nintendo;
  }

  delete file;

  return 0;
}

