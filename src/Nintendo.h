/**
 *  unemulate
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2018 by Michael Kohn
 *
 */

#ifndef NINTENDO
#define NINTENDO

#include <stdio.h>
#include <stdlib.h>

#include "FileNintendo.h"

class Nintendo
{
public:
  Nintendo();
  ~Nintendo();
  int parse(FileNintendo *file);

private:
};

#endif

