#!/usr/bin/env python

import sys

if len(sys.argv) != 2:
  print "Usage: " + sys.argv[0] + " table.c"
  sys.exit(0)

fp = open(sys.argv[1], "rb")

count = 0

for line in fp:
  line = line.strip()
  if not line.startswith("/* 0"): continue

  tokens = line.split("*/")

  opcode = int(tokens[0].replace("/*","").strip(), 16)

  tokens = tokens[1].replace("{","").replace("}","").split(",")
  tokens = [ item.strip() for item in tokens ]

  if count != opcode: print "Missed " + int(count)

  if tokens[0] == "M65XX_ERROR":
    print "  { \"\",     0, 0 }, // %02x" % count
  else:
    instruction = "\"" + tokens[0].replace("M65XX_","").lower() + "\","

    branch = 0

    if tokens[1] == "OP_NONE": size = 1
    elif tokens[1] == "OP_IMMEDIATE": size = 2
    elif tokens[1] == "OP_X_INDIRECT8": size = 2
    elif tokens[1] == "OP_INDIRECT8_Y": size = 2
    elif tokens[1] == "OP_INDEXED8_X": size = 2;
    elif tokens[1] == "OP_INDEXED8_Y": size = 2;
    elif tokens[1] == "OP_ADDRESS8": size = 2
    elif tokens[1] == "OP_ADDRESS16": size = 3
    elif tokens[1] == "OP_RELATIVE": size = 2; branch = 1
    elif tokens[1] == "OP_ADDRESS8_RELATIVE": size = 0;
    elif tokens[1] == "OP_INDIRECT8": size = 2;
    elif tokens[1] == "OP_INDIRECT16": size = 3;
    elif tokens[1] == "OP_INDEXED16_X": size = 3;
    elif tokens[1] == "OP_INDEXED16_Y": size = 3;
    elif tokens[1] == "OP_X_INDIRECT16": size = 3;
    else:
      print "Unknown op type: " + tokens[1]
      break

    if "JSR" in tokens[0]: branch = 2
    if "JMP" in tokens[0]:
      if tokens[1] == "OP_ADDRESS16": branch = 2
      if tokens[1] == "OP_INDIRECT16": branch = 3
      if tokens[1] == "OP_X_INDIRECT16": branch = 3

    print "  { %-7s %d, %s }, // %02x" % (instruction, size, branch, count)

  count += 1

fp.close()

