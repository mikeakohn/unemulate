#!/usr/bin/env python

import sys

op_none = {
  "nop": "nop",
  "asl": "asl.b #1, d0",
  "dex": "subq.b #1, a0",
  "dey": "subq.b #1, a1",
  "dec": "subq.b #1, d0",
  "inx": "addq.b #1, a0",
  "iny": "addq.b #1, a1",
  "inc": "addq.b #1, d0",
  "lsl": "lsl.b #1, d0",
  "lsr": "lsr.b #1, d0",
  "pha": "movea.l d0, -(a7)",
  "phx": "movea.l a0, -(a7)",
  "phy": "movea.l a1, -(a7)",
  "pla": "movea.l -(a7), d0",
  "plx": "movea.l -(a7), a0",
  "ply": "movea.l -(a7), a1",
  "rol": "rol.b #1, d0",
  "rol": "rol.b #1, d0",
  "rts": "rts",
  "tax": "move.b d0, a0",
  "tay": "move.b d0, a1",
  "txa": "move.b a0, d0",
  "tya": "move.b a1, d0",
}

equiv = {
  "add": "add",
  "sub": "sub",
}

jumps = {
  "jmp": "jmp",
  "jsr": "jsr",
}

load = {
  "lda" : [ "move.b", "d0" ],
  "ldx" : [ "movea", "a0" ],
  "ldy" : [ "movea", "a1" ],
  "sta" : [ "move.b", "d0" ],
  "stx" : [ "movea", "a0" ],
  "sty" : [ "movea", "a1" ],
}

if len(sys.argv) != 2:
  print "Usage: " + sys.argv[0] + " table.c"
  sys.exit(0)

fp = open(sys.argv[1], "rb")

count = 0
generate = []

for line in fp:
  line = line.strip()
  if not line.startswith("/* 0"): continue

  tokens = line.split("*/")

  opcode = int(tokens[0].replace("/*","").strip(), 16)

  tokens = tokens[1].replace("{","").replace("}","").split(",")
  tokens = [ item.strip() for item in tokens ]

  if count != opcode: print "Missed " + int(count)

  if tokens[0] == "M65XX_ERROR":
    count += 1
    continue
  else:
    instruction = tokens[0].replace("M65XX_","").lower()

    if tokens[1] == "OP_ADDRESS8_RELATIVE":
      count += 1
      continue

    print "      case 0x%02x:" % count

    branch = 0
    disasm = "oops"
    name = "oops"
    function = ""

    if tokens[1] == "OP_NONE":
      size = 1
      disasm = instruction
      name = instruction
    elif tokens[1] == "OP_IMMEDIATE":
      size = 2
      disasm = instruction + " #0x%02x"
      name = instruction + "_imm"
    elif tokens[1] == "OP_X_INDIRECT8":
      size = 2
      disasm = instruction + " (0x%02x, x)"
      name = instruction + "_x_indirect8"
    elif tokens[1] == "OP_INDIRECT8_Y":
      size = 2
      disasm = instruction + " (0x%02x), y"
      name = instruction + "_indirect8_y"
    elif tokens[1] == "OP_INDEXED8_X":
      size = 2;
      disasm = instruction + " 0x%02x, x"
      name = instruction + "_indexed8_x"
    elif tokens[1] == "OP_INDEXED8_Y":
      size = 2;
      disasm = instruction + " 0x%02x, y"
      name = instruction + "_indexed8_y"
    elif tokens[1] == "OP_ADDRESS8":
      size = 2
      disasm = instruction + " 0x%02x"
      name = instruction + "_address8"
    elif tokens[1] == "OP_ADDRESS16":
      size = 3
      disasm = instruction + " 0x%04x"
      name = instruction + "_address16"
    elif tokens[1] == "OP_RELATIVE":
      branch = 1
      size = 2;
      name = instruction + "_branch"
      disasm = instruction + " 0x%04x (offset=%d)"
    elif tokens[1] == "OP_ADDRESS8_RELATIVE":
      branch = 1
      size = 1;
      disasm = instruction + " error"
      name = instruction + "_error"
    elif tokens[1] == "OP_INDIRECT8":
      size = 2;
      disasm = instruction + " (0x%02x)"
      name = instruction + "_indirect8"
    elif tokens[1] == "OP_INDIRECT16":
      size = 3;
      disasm = instruction + " (0x%04x)"
      name = instruction + "_indirect16"
    elif tokens[1] == "OP_INDEXED16_X":
      size = 3;
      disasm = instruction + " 0x%04x, x"
      name = instruction + "_indexed16_x"
    elif tokens[1] == "OP_INDEXED16_Y":
      size = 3;
      disasm = instruction + " 0x%04x, y"
      name = instruction + "_indexed16_y"
    elif tokens[1] == "OP_X_INDIRECT16":
      size = 3;
      disasm = instruction + " (0x%04x, x)"
      name = instruction + "_x_indirect16"
    else:
      print "Unknown op type: " + tokens[1]
      break

    if size == 1:
      print "        fprintf(fp, \"  ;; %04x: " + disasm + "\\n\", start);"
      function = "append_" + name + "()"
      print "        generate->append_" + name + "();"
    elif size == 2:
      if branch == 0:
        print "        value = code[offset + 1];"
        print "        fprintf(fp, \"  ;; %04x: " + disasm + "\\n\", start, value);"
        print "        generate->append_" + name + "(value);"
        function = "append_" + name + "(int value)"
      else:
        print "        value = ((int8_t)code[offset + 1]);"
        print "        fprintf(fp, \"  ;; %04x: " + disasm + "\\n\", start, start + 2 + value, value);"
        print "        generate->append_" + name + "(start + 2 + value);"
        function = "append_" + name + "(int value)"
    elif size == 3:
      print "        value = code[offset + 1] | (code[offset + 2] << 8);"
      print "        fprintf(fp, \"  ;; %04x: " + disasm + "\\n\", start, value);"
      print "        generate->append_" + name + "(value);"
      function = "append_" + name + "(int value)"

    print "        start += " + str(size) + ";"
    print "        break;"

    generate.append([ function, tokens[1], instruction ])

  count += 1

fp.close()

print

for function in generate:
  print "  int " + function[0] + ";"

fp = open("src/Generate68000.cxx", "rb")
out = open("temp.cxx", "wb")

skip = 0

for line in fp:
  if skip == 1:
    if not "ADD STOP" in line: continue
    skip = 0

  out.write(line)

  if "ADD START" in line:

    skip = 1

    for function in generate:
      out.write("int Generate68000::" + function[0] + "\n")
      out.write("{\n")

      if function[1] == "OP_NONE":
        if function[2] in op_none:
          out.write("  fprintf(fp, \"  " + op_none[function[2]] + "\\n\");\n")
          out.write("  return 0;\n")
        else:
          out.write("  return -1;\n")
      elif function[2] in equiv:
        if function[1] == "OP_IMMEDIATE":
          out.write("  fprintf(fp, \"  " + equiv[function[2]] + \
                    "i.b #0x%02x, d0\\n\", value);\n")
          out.write("  return 0;\n")
        else:
          out.write("  return -1;\n")
      elif function[2] in load:
        instr = load[function[2]][0]
        reg = load[function[2]][1]

        name = function[2]

        if name[0] == "l": is_load = 1
        else: is_load = 0

        if function[1] == "OP_IMMEDIATE":
          out.write("  fprintf(fp, \"  " + instr + " #0x%02x, " + reg + "\\n\", value & 0xff);\n")
          out.write("  return 0;\n")
        elif function[1] == "OP_ADDRESS8":
          if is_load == 1:
            out.write("  fprintf(fp, \"  " + instr + " (0x%02x,a2), " + reg + "\\n\", value & 0xff);\n")
          else:
            out.write("  fprintf(fp, \"  " + instr + " " + reg + ", (0x%02x,a2)\\n\", value & 0xff);\n")
          out.write("  return 0;\n")
        elif function[1] == "OP_ADDRESS16":
          if is_load == 1:
            out.write("  fprintf(fp, \"  " + instr + " (0x%04x,a2), " + reg + "\\n\", value);\n")
          else:
            out.write("  fprintf(fp, \"  " + instr + " " + reg + ", (0x%04x,a2)\\n\", value);\n")
          out.write("  return 0;\n")
        else:
          out.write("  return -1;\n")
      elif function[1] == "OP_RELATIVE":
        out.write("  fprintf(fp, \"  " + function[2] + " label_%04x_%d\\n\", value, bank);\n")
        out.write("  return 0;\n")
      elif function[2] in jumps:
        if function[1] == "OP_ADDRESS16":
          out.write("  fprintf(fp, \"  " + function[2] + " label_%04x_%d\\n\", value, bank);\n")
          out.write("  return 0;\n")
        else:
          out.write("  return -1;\n")
      else:
        out.write("  return -1;\n")

      out.write( "}\n\n")

fp.close()

