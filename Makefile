
default:
	@+make -C build

.PHONY: tests
tests:
	@cd tests && bash run_tests.sh

clean:
	@rm -f unemulate build/*.o *.asm *.lst *.hex *.prg
	@echo "Clean!"

distclean: clean

